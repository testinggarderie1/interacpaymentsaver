const Datastore = require('@google-cloud/datastore');
const customDatastore = require('DataStore');

const datastore = new Datastore({
  projectId: 'manager-218900',
});

async function saveInteracPaymentWithoutPaymentSheet(interacPayment){
  await customDatastore.addEntity({kind : 'AdvancedInteracPayment', interacMessageId: interacPayment.messageID, moneySent: interacPayment.moneySent,
    sender: interacPayment.sender, dateOfPayment: interacPayment.dateOfPayment, encodedMessageContent: interacPayment.encodedMessageContent,
    familyId: interacPayment.familyId});
}
async function saveInteracPayment(interacPayment, lastUnPaidChildBillPeriod){
  await customDatastore.addEntity({kind : 'InteracPayment', interacMessageId: interacPayment.messageID, moneySent: interacPayment.moneySent,
    sender: interacPayment.sender, dateOfPayment: interacPayment.dateOfPayment, encodedMessageContent: interacPayment.encodedMessageContent,
    paymentSheetId: `PaymentSheet-${lastUnPaidChildBillPeriod.month}-${lastUnPaidChildBillPeriod.year}`,
    familyBillId: `FamilyBill-${lastUnPaidChildBillPeriod.familyId}`,
    childBillId: `ChildBill-${lastUnPaidChildBillPeriod.childId}`});

}

async function doesFamilyExist(familyId){
  if(!familyId){
    throw new Error(`familyID ${familyId} is not valid`);
  }
  const entityKey = datastore.key(['Family' , familyId]);
  const [entity] = await datastore.get(entityKey);
  if(entity){
    console.log(`retreived : ${entity[datastore.KEY].kind} - ${entity[datastore.KEY].name}`);
    return true
  }
  return false;
}

async function getPaymentsObjectByFamilyId(familyId){

  const familyBillObjects = await getFamilyBillsWithPeriods(familyId);

  for(let familyBillObject of familyBillObjects){
    const paymentSheetId = familyBillObject.paymentSheet[Datastore.KEY].name;
    const familyBillId = familyBillObject.familyBill[Datastore.KEY].name;

    const childBills = await getEntityByParents(['PaymentSheet',paymentSheetId, 'FamilyBill', familyBillId], 'ChildBill');

    familyBillObject.childBills = [];
    for(let childBill of childBills){
      const childBillId = childBill[Datastore.KEY].name;
      const interacPayments = await getEntityByParents(['PaymentSheet',paymentSheetId, 'FamilyBill', familyBillId,
      'ChildBill',childBillId], 'InteracPayment');

      childBill.interacPayments = interacPayments;

      familyBillObject.childBills.push(childBill);

    }
  }

  return familyBillObjects;



}

async function getEntityByParents(parents, kind){
  const query = datastore.createQuery(kind);
  const ancestorKey = datastore.key(parents);
  query.hasAncestor(ancestorKey);
  const [entities] = await datastore.runQuery(query);
  console.log(`${kind}s:`);
  entities.forEach(entity => console.log(entity));

  return entities;
}

async function getFamilyBillsWithPeriods(familyId){
  const familyBills = await getFamilyBills(familyId);
  let familyBillsWithPeriods = [];
  for(const familyBill of familyBills){
    const paymentSheetId = familyBill[Datastore.KEY].parent.name;
    const paymentSheet = await getEntityByKey(['PaymentSheet', paymentSheetId]);
    familyBillsWithPeriods.push({
      paymentSheet: paymentSheet,
      familyBill: familyBill
    });
  }
  return familyBillsWithPeriods;
}

async function getFamilyBills(familyId) {
  const query = datastore.createQuery('FamilyBill');
  query.filter('familyId', '=', familyId);
  const [entities] = await datastore.runQuery(query);
  console.log(`FamilyBills:`);
  entities.forEach(entity => console.log(entity));

  return entities;
}

async function getEntityByKey(key){
  const entityKey = datastore.key(key);
  const [entity] = await datastore.get(entityKey);
  if(entity){
    console.log(`retreived : ${entity[datastore.KEY].kind} - ${entity[datastore.KEY].name}`);
    return entity;
  }
  return null;
}

async function messageHaveBeenTreated(messageId){
  const query = datastore.createQuery('InteracPayment');
  query.filter('interacMessageId', '=', messageId);
  const [entities] = await datastore.runQuery(query);
  console.log(`FamilyBills:`);
  entities.forEach(entity => console.log(entity));
  if(entities.length > 0){
    return true;
  }
  return false;
}

module.exports = {getPaymentsObjectByFamilyId , saveInteracPayment, doesFamilyExist,
  saveInteracPaymentWithoutPaymentSheet, messageHaveBeenTreated};
