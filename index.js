const InteracMessageSaver = require('./InteracMessagesSaver');
exports.interacPaymentSaver = async (event, callback) => {
  const pubsubMessage = event.data;
  const messages = JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString());
  try{
    await InteracMessageSaver.saveInteracPayments(messages);
  }catch (err) {
    console.error(err);
  }
};
