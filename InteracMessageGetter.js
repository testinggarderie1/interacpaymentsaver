const pubsub = require('@google-cloud/pubsub');

const client = new pubsub.v1.SubscriberClient({});
const formattedSubscription = client.subscriptionPath('manager-218900', 'GetNewInteracMessages');

async function acknowledgeMessages(ackIds){
  const request = {
    subscription: formattedSubscription,
    ackIds: ackIds,
  };
  await client.acknowledge(request)
  .then(`messages : ${ackIds} acknoledged`)
  .catch(err => {
    console.error(err);
  });
}

async function getInteracMessagesFromPubSub(){
  return new Promise(async (resolve, reject) => {
    const maxMessages = 100;
    const request = {
      subscription: formattedSubscription,
      maxMessages: maxMessages,
    };
    await client.pull(request)
    .then(async (responses) => {
      let interacMessages = [];
      for(const response of responses[0].receivedMessages){
        const pubsubSubMessage = JSON.parse(Buffer.from(response.message.data).toString());
        interacMessages.push({messages: pubsubSubMessage.messages, ackId : response.ackId});
      }
      //interacMessages.forEach(message => console.log(message));

      resolve(interacMessages);
    })
    .catch(err => {
      console.error(err);
    });
  });

};

module.exports = {acknowledgeMessages, getInteracMessagesFromPubSub}




