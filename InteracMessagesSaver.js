const InteracMessagesGettter =  require('./InteracMessageGetter');
const PaymentCalculator = require('./PaymentCalculator');
const DataBase = require('./DataBase');

async function saveInteracPayments(interacPayments){
  if(!interacPayments){
    throw new Error('no New Interac Messages');
  }

  for(const interacPayment of interacPayments){
    for(const message of interacPayment.messages){
      if(!await DataBase.messageHaveBeenTreated(message.messageID)){
        await PaymentCalculator.decidePayment(message);
      }else{
        console.warn(`interacPayment ${message.messageID} has been treated`);
      }
    }
    await InteracMessagesGettter.acknowledgeMessages(interacPayment.ackId);
  }



}

module.exports = {saveInteracPayments};
