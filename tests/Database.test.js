const Database = require('../interacPaymentsSaver/DataBase');

describe('# Database', () => {
  it(' should get whole family by familyID', async () => {
    await Database.getWholeFamilyByFamilyId('genie1234');
  });

  it(' should get all familyBIlls by familyID', async () => {
    await Database.getFamilyBillsWithPeriods('genie1234');
  });

  it(' should sort family periods', async () => {
    const familyBillsWithPeriods = [
      {
        period: {year : 2019, month : 8},
        familyBill: 'Whatever'
      },
      {
        period: {year : 2019, month : 7},
        familyBill: 'Whatever'
      },
      {
        period: {year : 2019, month : 6},
        familyBill: 'Whatever'
      },
      {
        period: {year : 2019, month : 5},
        familyBill: 'Whatever'
      },
    ];
    const result = await Database.sortFamilyBillsByDate(familyBillsWithPeriods);
    console.log(result);
  });

  it(' should get all childrensBIlls by familyBill', async () => {
    const result = await Database.getChildrensBillByFamilyBillId('Familybill-genie1234');
    console.log(result);
  });

  it(' should get all InteracPayments by childBillId', async () => {
    const result = await Database.getInteracPaymentByChildBillId('ChildBill-Mahjoubi Taha');
    console.log(result);
  });

  it(' should get familyBillObjects by childBillId', async () => {
    const result = await Database.getPaymentsObjectByFamilyId('genie234');
    console.log(result);
  });
});