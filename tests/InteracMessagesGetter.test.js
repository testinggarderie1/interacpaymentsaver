
const InteracMessagesGetter = require('../interacPaymentsSaver/InteracMessageGetter');

describe('# interacMessagesGetter', () => {
  it('should get the interac messagefrom pubsub', async () => {
    const result = await InteracMessagesGetter.getInteracMessagesFromPubSub();
    expect(typeof result).toBe('object');
  });
});