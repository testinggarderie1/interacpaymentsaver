const InteracMasssageSaver = require('../interacPaymentsSaver/InteracMessagesSaver');
const InteracMessageGetter = require('../interacPaymentsSaver/InteracMessageGetter');
const interacPayments = require('../tests/mocks/pojos.mocks');


describe('# InteracPaymentSaver', () => {
  it(' should save payments who come from pubsub', async () => {
    InteracMessageGetter.getInteracMessagesFromPubSub = jest.fn(() => {return interacPayments.interacPayment});
    await InteracMasssageSaver.saveInteracPayments();
  });
});