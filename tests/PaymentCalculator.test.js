const PaymentCalculator = require('../interacPaymentsSaver/PaymentCalculator');

const InteracPaymentInfosTest = { familyId : 'genie234', moneySent : '700', sender : 'Sarra',
  dateOfPayment : '2019-02-23T09:30:16.768-04:00', messageID : '1234', encodedMessageContent : 'dcdccfewsfd' };

describe('# PaymentCalculator', () => {
  it(' should calculate give back the last unpaid period', async () => {
    const result = await PaymentCalculator.getUnpaidPeriodsOfFamily('genie234');
    console.log(result);
  });

  it(' should decide how to log the interac payment', async () => {
    const result = await PaymentCalculator.decidePayment(InteracPaymentInfosTest);
    console.log(result);
  });
});