const DataBase = require('./DataBase');

async function decidePayment(interacPayment){
  const familyId = interacPayment.familyId;

  if(await DataBase.doesFamilyExist(familyId)){
    throw new Error(`Family ${familyId} does not exist`);
  }

  const paymentAmount = parseInt(interacPayment.moneySent);
  const lastUnPaidChildBillPeriod = getLastUnpaidPeriod(await getUnpaidChildBillOfFamily(familyId));

  if(!lastUnPaidChildBillPeriod && paymentAmount){
    await logPayment(interacPayment);
    console.error('no childBill for recent payment')
    return
  }

  const amountToPay = lastUnPaidChildBillPeriod.amountToBePayed;

  if(amountToPay <= paymentAmount){

    interacPayment.moneySent = amountToPay;
    await logPayment(interacPayment, lastUnPaidChildBillPeriod);

    const difference = paymentAmount - amountToPay;

    if(0 < difference){
      interacPayment.moneySent = difference;
      await decidePayment(interacPayment);
    }
  }

  if(amountToPay > paymentAmount){
    await logPayment(interacPayment, lastUnPaidChildBillPeriod);
  }

}

async function logPayment(interacPayment , lastUnPaidChildBillPeriod){
  if(!lastUnPaidChildBillPeriod){
    await DataBase.saveInteracPaymentWithoutPaymentSheet(interacPayment);
  }
  if(lastUnPaidChildBillPeriod){
    await DataBase.saveInteracPayment(interacPayment, lastUnPaidChildBillPeriod);
  }
  await new Promise(resolve => setTimeout(resolve, 5000));
}

async function getUnpaidChildBillOfFamily(familyID) {
  const paymentObject = await DataBase.getPaymentsObjectByFamilyId(familyID);
  let unpaidChildBill = [];
  for(const paymentPeriod of paymentObject){
    for(const childBill of paymentPeriod.childBills){
      const amountToBePayed = childBill.amount;
      const amountPayed = getAmmontPayedForChildForPeriod(childBill);

      if(amountPayed < amountToBePayed){
        childBill.amountToBePayed = amountToBePayed - amountPayed;
        childBill.amount = amountToBePayed;
        childBill.amountPayed = amountPayed;
        childBill.familyId = paymentPeriod.familyBill.familyId;
        childBill.year = paymentPeriod.paymentSheet.year;
        childBill.month = paymentPeriod.paymentSheet.month;
        unpaidChildBill.push(childBill);
      }
    }
  }
  return unpaidChildBill;

}

function getLastUnpaidPeriod(unpaidPeriods){
  let lastUnpaidPeriodIndex = 0;
  for(let i = 0; i<unpaidPeriods.length; i++) {

    const unpaidPeriodsDate = new Date(unpaidPeriods[i].year, unpaidPeriods[i].month);
    const lastUnpaidPeriodsDate = new Date(unpaidPeriods[lastUnpaidPeriodIndex].year,
      unpaidPeriods[lastUnpaidPeriodIndex].month);

    if (unpaidPeriodsDate < lastUnpaidPeriodsDate) {
      lastUnpaidPeriodIndex = i;
    }
  }
  return unpaidPeriods[lastUnpaidPeriodIndex];
}

function getAmmontPayedForChildForPeriod(childBill){
  let amountPayed = 0;
  for(const interacPayment of childBill.interacPayments){
    amountPayed += parseInt(interacPayment.moneySent);
  }
  return amountPayed;
}

module.exports = {getUnpaidPeriodsOfFamily: getUnpaidChildBillOfFamily , decidePayment};